﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace trySelect2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult TestApi(string q)
        {
            List<QueryTag> options = new List<QueryTag>();
            options.Add(new QueryTag { id = 1, text = "orange" });
            options.Add(new QueryTag { id = 2, text = "apple" });
            options.Add(new QueryTag { id = 3, text = "grape" });
            options.Add(new QueryTag { id = 4, text = "cherry" });
            options.Add(new QueryTag { id = 5, text = "peach" });
            var QueryOptions = (from qo in options
                                where qo.text.Contains(q)
                                select new { id = qo.id, text = qo.text }).ToList();

            return Json(QueryOptions, JsonRequestBehavior.AllowGet);
        }

    }

    public class QueryTag
    {
        public int id { get; set; }
        public string text { get; set; }
    }
}